﻿namespace _2WheelCharts
{
    public class Trip
    {
        public double? Distance { get; set; }
        public double? Ascent { get; set; }
        public double? TTIS { get; set; }
        public double? AvgAlt { get; set; }
        public double? MaxAlt { get; set; }
        public double? AvgSpd { get; set; }
        public double? MaxSpd { get; set; }
        public double? Odo { get; set; }
    }
}