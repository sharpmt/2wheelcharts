﻿using System.Collections.Generic;

namespace _2WheelCharts
{
    internal class ImportedFile
    {
        public Params Params = new Params();
        public List<HRData> HRData = new List<HRData>();
        public Trip Trip = new Trip();
        public IntNotes IntNotes = new IntNotes();
        public string FileName { get; set; } = string.Empty;

    }
}