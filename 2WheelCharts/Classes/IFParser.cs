﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _2WheelCharts
{
    internal class IFParser
    {
        private ImportedFile file;

        public IFParser(ImportedFile file)
        {
            this.file = file;
        }

        /// <summary>
        /// Parses the parameter information that is originally checked in form 1
        /// </summary>
        /// <param name="paramData"></param>
        /// <returns></returns>
        public Params ParseParams(string paramData)
        {
            List<string> data = new List<string>();
            Params param = new Params();

            string[] split = paramData.Split(new Char[] { '\n' });

            #region check to see if value contains

            foreach (string a in split)
            {
                if (a.Contains("Version="))
                {
                    param.Version = Convert.ToInt32(a.Split('=').Last());
                }
                if (a.Contains("Monitor="))
                {
                    param.Monitor = a.Split('=').Last();

                    #region case/switch to determing model data is taken from

                    switch (param.Monitor)
                    {
                        case "1":
                            param.Monitor = "Polar Sport Tester / Vantage XL";
                            break;

                        case "2":
                            param.Monitor = "Polar Vantage NV (VNV)";
                            break;

                        case "3":
                            param.Monitor = "Polar Accurex Plus";
                            break;

                        case "4":
                            param.Monitor = "Polar XTrainer Plus";
                            break;

                        case "6":
                            param.Monitor = "Polar S520";
                            break;

                        case "7":
                            param.Monitor = "Polar Coach";
                            break;

                        case "8":
                            param.Monitor = "Polar S210";
                            break;

                        case "9":
                            param.Monitor = "Polar S410";
                            break;

                        case "10":
                            param.Monitor = "Polar S510";
                            break;

                        case "11":
                            param.Monitor = "Polar S610 / S610i";
                            break;

                        case "12":
                            param.Monitor = "Polar S710 / S710i / S720i";
                            break;

                        case "13":
                            param.Monitor = "Polar S810 / S810i";
                            break;

                        case "15":
                            param.Monitor = "Polar E600";
                            break;

                        case "20":
                            param.Monitor = "Polar AXN500";
                            break;

                        case "21":
                            param.Monitor = "Polar AXN700";
                            break;

                        case "22":
                            param.Monitor = "Polar S625X / S725X";
                            break;

                        case "23":
                            param.Monitor = "Polar S725";
                            break;

                        case "33":
                            param.Monitor = "Polar CS400";
                            break;

                        case "34":
                            param.Monitor = "Polar CS600X";
                            break;

                        case "35":
                            param.Monitor = "Polar CS600";
                            break;

                        case "36":
                            param.Monitor = "Polar RS400";
                            break;

                        case "37":
                            param.Monitor = "Polar RS800";
                            break;

                        case "38":
                            param.Monitor = "Polar RS800X";
                            break;

                        default:
                            param.Monitor = "Monitor name not found";
                            break;
                    }

                    #endregion case/switch to determing model data is taken from
                }
                if (a.Contains("SMode="))
                {
                    param.SMode = a.Split('=').Last();

                    #region Parses data to indivudual units from SMode

                    Char[] SModeValues = param.SMode.ToCharArray();

                    Boolean[] value = new Boolean[SModeValues.Length];

                    int i = 0;
                    foreach (var indicator in SModeValues)
                    {
                        value[i] = false;
                        if (indicator.ToString() == "1")
                        {
                            value[i] = true;
                        }
                        i++;
                    }

                    param.sMode.SpeedAvailable = value[0];
                    param.sMode.CadenceAvailable = value[1];
                    param.sMode.AltitudeAvailable = value[2];
                    param.sMode.PowerAvailable = value[3];
                    param.sMode.LRBAvailable = value[4];
                    param.sMode.PowerPedallingIndexAvailable = value[5];
                    param.sMode.HRAvailable = value[6];
                    param.sMode.MetricOrImperial = value[7];
                    // Boolean SModeAirPressure would be value[8]

                    #endregion Parses data to indivudual units from SMode

                }
                if (a.Contains("Date="))
                {
                    param.Date = a.Split('=').Last();

                    #region parse date into a readable format

                    string year = param.Date.Substring(0, 4);
                    string month = param.Date.Substring(4, 2);
                    string day = param.Date.Substring(6, 2);
                    param.Date = day + "/" + month + "/" + year;

                    #endregion parse date into a readable format
                }
                if (a.Contains("StartTime="))
                {
                    param.StartTime = TimeSpan.Parse(a.Split('=').Last());
                    int hrs = param.StartTime.Hours;
                    int mins = param.StartTime.Minutes;
                    int secs = param.StartTime.Seconds;
                    //param.StartTime.ToString(hrs + ":" + mins + ":" + secs);
                }
                if (a.Contains("Length="))
                {
                    param.Length = TimeSpan.Parse(a.Split('=').Last());
                }
                if (a.Contains("Interval="))
                {
                    param.Interval = Convert.ToInt32(a.Split('=').Last());
                }
                if (a.Contains("Upper1="))
                {
                    param.Upper1 = Convert.ToInt32(a.Split('=').Last());
                }
                if (a.Contains("Lower1="))
                {
                    param.Lower1 = Convert.ToInt32(a.Split('=').Last());
                }
                if (a.Contains("Upper2="))
                {
                    param.Upper2 = Convert.ToInt32(a.Split('=').Last());
                }
                if (a.Contains("Lower2="))
                {
                    param.Lower2 = Convert.ToInt32(a.Split('=').Last());
                }
                if (a.Contains("Upper3="))
                {
                    param.Upper3 = Convert.ToInt32(a.Split('=').Last());
                }
                if (a.Contains("Lower3="))
                {
                    param.Lower3 = Convert.ToInt32(a.Split('=').Last());
                }
                if (a.Contains("Timer1="))
                {
                    param.Timer1 = TimeSpan.Parse(a.Split('=').Last());
                }
                if (a.Contains("Timer2="))
                {
                    param.Timer2 = TimeSpan.Parse(a.Split('=').Last());
                }
                if (a.Contains("Timer3="))
                {
                    param.Timer3 = TimeSpan.Parse(a.Split('=').Last());
                }
                if (a.Contains("ActiveLimit="))
                {
                    param.ActiveLimit = Convert.ToInt32(a.Split('=').Last());
                }
                if (a.Contains("MaxHR="))
                {
                    param.MaxHR = Convert.ToInt32(a.Split('=').Last());
                }
                if (a.Contains("RestHR="))
                {
                    param.RestHR = Convert.ToInt32(a.Split('=').Last());
                }
                if (a.Contains("StartDelay="))
                {
                    param.StartDelay = Convert.ToInt32(a.Split('=').Last());
                }
                if (a.Contains("VO2max="))
                {
                    param.V02max = Convert.ToInt32(a.Split('=').Last());
                }
                if (a.Contains("Weight="))
                {
                    param.Weight = Convert.ToInt32(a.Split('=').Last());
                }
            }

            #endregion check to see if value contains

            return param;
        }

        /// <summary>
        /// Parses the Int Times information that is originally checked in form 1
        /// </summary>
        /// <param name="intTimesData"></param>
        /// <returns></returns>
        public IntTimes ParseIntTimes(string intTimesData)
        {
            List<string> data = new List<string>();
            IntTimes intTimes = new IntTimes();

            string[] split = intTimesData.Split(new Char[] { '\n' });

            #region check to see if value contains

            foreach (string a in split)
            {
            }

            #endregion check to see if value contains

            return intTimes;
        }

        /// <summary>
        /// Parses the HR data that is originally checked in form 1 and assigns it in memory
        /// </summary>
        /// <param name="hrd"></param>
        /// <returns></returns>
        public List<HRData> ParseHRData(string hrd)
        {
            #region Split the list by new line then by individual character

            List<HRData> data = new List<HRData>();
            string[] xSplit = hrd.Split('\n');

            double timer = 0;

            foreach (var line in xSplit)
            {
                HRData dataLine = new HRData();
                string[] LineSplit = line.Split('\t');
                if (!string.IsNullOrEmpty(line))
                {
                    timer += file.Params.Interval;

                    dataLine.HeartRate = Convert.ToInt32(LineSplit[0]);

                    if (file.Params.sMode.MetricOrImperial == Convert.ToBoolean(1))
                    {
                        dataLine.Speed = Convert.ToInt32(LineSplit[1]) / 10 * 0.60934;
                    }
                    else
                    {
                        dataLine.Speed = Convert.ToInt32(LineSplit[1]) / 10;
                    }

                    dataLine.Cadence = Convert.ToInt32(LineSplit[2]);
                    dataLine.Altitude = Convert.ToInt32(LineSplit[3]);

                    int AltMin = LineSplit[3].Min();
                    int AltMax = LineSplit[3].Max();

                    dataLine.Power = Convert.ToInt32(LineSplit[4]);
                    dataLine.PBPIndex = Convert.ToInt32(LineSplit[5]);

                    // Converts data from byte to int 
                    byte[] intBytes = BitConverter.GetBytes(Convert.ToInt32(dataLine.PBPIndex));

                    dataLine.PBL = intBytes[0];
                    dataLine.PBR = 100 - dataLine.PBL;

                    Array.Clear(intBytes, 0, intBytes.Length);

                    dataLine.ParsedTime = timer;
                    dataLine.ParsedTimeFormatted = TimeSpan.FromSeconds(timer);

                    data.Add(dataLine);
                }
                
            }
            return data;
            #endregion Split the list by new line then by individual character
        }

        /// <summary>
        /// Would parse any notes that are added into the cycle model
        /// </summary>
        /// <param name="IntNotes"></param>
        /// <returns></returns>
        public IntNotes ParseIntNotes(string IntNotes)
        {
            IntNotes number = new IntNotes();

            string[] ints = IntNotes.Split(new Char[] { '\n' });

            foreach (string n in ints)
            {
                if (n != "")
                {
                    number.Numbers = ints.Length - 2;
                }
            }
            return number;
        }

        /// <summary>
        /// Parses and stores the trip data to be used in future needs
        /// </summary>
        /// <param name="Trip"></param>
        /// <returns></returns>
        public Trip ParseTrip(string Trip)
        {
            List<string> data = new List<string>();
            Trip trip = new Trip();

            string[] tripitems = Trip.Split(new Char[] { '\n' });
            tripitems.ToList();

            foreach (string item in tripitems)
            {
                if (!string.IsNullOrEmpty(item))
                {
                    if (file.Params.sMode.MetricOrImperial == Convert.ToBoolean(1))
                    {
                        trip.Distance = Convert.ToInt32(tripitems[0]) / 10 * 0.60934;
                        trip.AvgSpd = Convert.ToInt32(tripitems[5]) / 10 * 0.60934;
                        trip.MaxSpd = Convert.ToInt32(tripitems[6]) / 10 * 0.60934;
                    }
                    else
                    {
                        trip.Distance = Convert.ToInt32(tripitems[0]) / 10;
                        trip.AvgSpd = Convert.ToInt32(tripitems[5]) / 10;
                        trip.MaxSpd = Convert.ToInt32(tripitems[6]) / 10;
                    }
                    trip.Ascent = Convert.ToInt32(tripitems[1]);
                    trip.TTIS = Convert.ToInt32(tripitems[2]);
                    trip.AvgAlt = Convert.ToInt32(tripitems[3]);
                    trip.MaxAlt = Convert.ToInt32(tripitems[4]);
                    trip.Odo = Convert.ToInt32(tripitems[7]);
                }
            }
            return trip;
        }
    }
}