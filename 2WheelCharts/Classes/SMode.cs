﻿namespace _2WheelCharts
{
    public class SMode
    {
        public bool HRAvailable { get; set; }

        /// <summary>
        /// 0 = off / not available, 1 = on / available
        /// </summary>
        public bool SpeedAvailable { get; set; }

        /// <summary>
        /// 0 = off / not available, 1 = on / available
        /// </summary>
        public bool CadenceAvailable { get; set; }

        /// <summary>
        /// 0 = off / not available, 1 = on / available
        /// </summary>
        public bool AltitudeAvailable { get; set; }

        /// <summary>
        /// 0 = off / not available, 1 = on / available
        /// </summary>
        public bool PowerAvailable { get; set; }

        /// <summary>
        /// 0 = off / not available, 1 = on / available
        /// </summary>
        public bool LRBAvailable { get; set; }

        /// <summary>
        /// 0 = off / not available, 1 = on / available
        /// </summary>
        public bool PowerPedallingIndexAvailable { get; set; }

        /// <summary>
        /// Metric = 0, Imperial = 1.
        /// </summary>
        public bool MetricOrImperial { get; set; }
    }
}