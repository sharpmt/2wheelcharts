﻿using System;

namespace _2WheelCharts
{
    public class HRData
    {
        public TimeSpan ParsedTimeFormatted { get; set; } = TimeSpan.MinValue;
        public double? ParsedTime { get; set; }
        public double? HeartRate { get; set; }
        public double? Speed { get; set; }
        public double? Cadence { get; set; }
        public double? Altitude { get; set; }
        public double? Power { get; set; }
        public double? PBPIndex { get; set; }
        public double? Length { get; set; }
        public double? PBL { get; set; }
        public double? PBR { get; set; }
        /* If version is 107
        public int AirPressure { get; set; }*/
    }
}