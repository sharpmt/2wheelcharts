﻿using System;

namespace _2WheelCharts
{
    public class Params
    {
        public int Version { get; set; }
        public string Monitor { get; set; }
        public string SMode { get; set; }
        public string Date { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan Length { get; set; }
        public int Interval { get; set; } = 0;
        public int Upper1 { get; set; }
        public int Lower1 { get; set; }
        public int Upper2 { get; set; }
        public int Lower2 { get; set; }
        public int Upper3 { get; set; }
        public int Lower3 { get; set; }
        public TimeSpan Timer1 { get; set; }
        public TimeSpan Timer2 { get; set; }
        public TimeSpan Timer3 { get; set; }
        public int ActiveLimit { get; set; }
        public int MaxHR { get; set; }
        public int RestHR { get; set; }
        public int StartDelay { get; set; }
        public int V02max { get; set; }
        public int Weight { get; set; }
        public SMode sMode { get; set; } = new SMode();
    }

    /* CASE AND SWITCH INFO FOR THE POLAR MONITOR MODEL
     *
            1 = Polar Sport Tester / Vantage XL
            2 = Polar Vantage NV (VNV)
            3 = Polar Accurex Plus
            4 = Polar XTrainer Plus
            6 = Polar S520
            7 = Polar Coach
            8 = Polar S210
            9 = Polar S410
            10 = Polar S510
            11 = Polar S610 / S610i
            12 = Polar S710 / S710i / S720i
            13 = Polar S810 / S810i
            15 = Polar E600
            20 = Polar AXN500
            21 = Polar AXN700
            22 = Polar S625X / S725X
            23 = Polar S725
            33 = Polar CS400
            34 = Polar CS600X
            35 = Polar CS600
            36 = Polar RS400
            37 = Polar RS800
            38 = Polar RS800X
     */
}