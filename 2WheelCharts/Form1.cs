﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using ZedGraph;

namespace _2WheelCharts
{
    public partial class Form1 : Form
    {
        private ImportedFile comparisonFile = null;
        private string ExtraData;
        private ImportedFile file = null;

        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Start of the opening and collection of the file, taking in basic data to be stored in classes once collected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Button1_Click(object sender, EventArgs e)
        {
            bool settingFile = false, settingComparisonFile = false;

            OpenFileDialog ofd = new OpenFileDialog
            {
                Title = "Open Text File",
                Filter = "HRM Files|*.hrm",
                InitialDirectory = @"C:\"
            };

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                if (file == null && comparisonFile == null)
                {
                    settingFile = true;
                    settingComparisonFile = false;
                }
                else if (file == null && comparisonFile != null)
                {
                    comparisonFile = null;
                    settingFile = true;
                    settingComparisonFile = false;
                }
                else if (file != null && comparisonFile == null)
                {
                    settingFile = false;
                    settingComparisonFile = true;
                }
                else if (file != null && comparisonFile != null)
                {
                    file = null;
                    comparisonFile = null;
                    settingFile = true;
                    settingComparisonFile = false;
                }

                string pathToFile = ofd.FileName;// saves the location of the selected object
                openComparisonToolStripMenuItem.Enabled = true;

                HandleFile(settingFile, settingComparisonFile, pathToFile);
            }
        }

        /// <summary>
        /// This method plots the graph where teh data from the parser is passed into it
        /// </summary>
        /// <param name="param"></param>
        /// <param name="data"></param>
        public void PlotGraph(Params param, List<HRData> data)
        {
            var graphControl = new ZedGraphControl();
            // Get a reference to the GraphPane instance in the ZedGraphControl
            GraphPane myPane = zg1.GraphPane;

            myPane.CurveList.Clear();
            myPane.GraphObjList.Clear();
            myPane.YAxisList.Clear();
            myPane.Y2AxisList.Clear();

            zg1.AxisChange();
            zg1.Invalidate();
            zg1.Refresh();

            // Create unique axis to X item
            YAxis YAxisHR = new YAxis();
            YAxis YAxisPowerBalance = new YAxis();
            YAxis YAxisSpeed = new YAxis();
            Y2Axis Y2AxisCadence = new Y2Axis();
            Y2Axis Y2AxisAltitude = new Y2Axis();
            Y2Axis Y2AxisPower = new Y2Axis();

            // Set the titles and axis labels
            myPane.Title.Text = "Data using " + param.Monitor + " from file \n" + file.FileName;//Add polar model here;
            myPane.XAxis.Title.Text = "Time, Seconds";

            //Setting of style
            YAxisHR.Color = Color.Red;
            YAxisHR.MinorTic.Color = Color.Transparent;
            YAxisHR.MajorTic.Color = Color.Transparent;
            YAxisHR.Scale.FontSpec.FontColor = Color.Red;
            YAxisHR.Scale.FontSpec.Size = 10;
            YAxisHR.IsAxisSegmentVisible = false;
            YAxisHR.IsVisible = false;
            YAxisHR.Title.IsVisible = false;

            //Setting of style
            YAxisSpeed.Color = Color.Green;
            YAxisSpeed.MinorTic.Color = Color.Transparent;
            YAxisSpeed.MajorTic.Color = Color.Transparent;
            YAxisSpeed.Scale.FontSpec.FontColor = Color.Green;
            YAxisSpeed.Scale.FontSpec.Size = 10;
            YAxisSpeed.IsAxisSegmentVisible = false;
            YAxisSpeed.IsVisible = false;
            YAxisSpeed.Title.IsVisible = false;

            //Setting of style
            Y2AxisCadence.Color = Color.Blue;
            Y2AxisCadence.MinorTic.Color = Color.Transparent;
            Y2AxisCadence.MajorTic.Color = Color.Transparent;
            Y2AxisCadence.Scale.FontSpec.FontColor = Color.Blue;
            Y2AxisCadence.Scale.FontSpec.Size = 10;
            Y2AxisCadence.IsAxisSegmentVisible = false;
            Y2AxisCadence.IsVisible = false;
            Y2AxisCadence.Title.IsVisible = false;

            //Setting of style
            Y2AxisAltitude.Color = Color.Orange;
            Y2AxisAltitude.MinorTic.Color = Color.Transparent;
            Y2AxisAltitude.MajorTic.Color = Color.Transparent;
            Y2AxisAltitude.Scale.FontSpec.FontColor = Color.Orange;
            Y2AxisAltitude.Scale.FontSpec.Size = 10;
            Y2AxisAltitude.IsAxisSegmentVisible = false;
            Y2AxisAltitude.IsVisible = false;
            Y2AxisAltitude.Title.IsVisible = false;

            //Setting of style
            Y2AxisPower.Color = Color.Indigo;
            Y2AxisPower.MinorTic.Color = Color.Transparent;
            Y2AxisPower.MajorTic.Color = Color.Transparent;
            Y2AxisPower.Scale.FontSpec.FontColor = Color.Indigo;
            Y2AxisPower.Scale.FontSpec.Size = 10;
            Y2AxisPower.IsAxisSegmentVisible = false;
            Y2AxisPower.IsVisible = false;
            Y2AxisPower.Title.IsVisible = false;

            //Setting of style
            YAxisSpeed.Color = Color.Green;
            YAxisSpeed.MinorTic.Color = Color.Transparent;
            YAxisSpeed.MajorTic.Color = Color.Transparent;
            YAxisSpeed.Scale.FontSpec.FontColor = Color.Green;
            YAxisSpeed.Scale.FontSpec.Size = 10;
            YAxisSpeed.IsAxisSegmentVisible = false;
            YAxisSpeed.IsVisible = false;
            YAxisSpeed.Title.IsVisible = false;

            //Setting of style
            YAxisPowerBalance.Color = Color.LightPink;
            YAxisPowerBalance.MinorTic.Color = Color.Transparent;
            YAxisPowerBalance.MajorTic.Color = Color.Transparent;
            YAxisPowerBalance.Scale.FontSpec.FontColor = Color.LightPink;
            YAxisPowerBalance.Scale.FontSpec.Size = 10;
            YAxisPowerBalance.IsAxisSegmentVisible = false;
            YAxisPowerBalance.IsVisible = false;
            YAxisPowerBalance.Title.IsVisible = false;

            if (file != null)
            {
                // Creating data points based on the file parser
                PointPairList HRList = new PointPairList();
                PointPairList SDList = new PointPairList();
                PointPairList CDList = new PointPairList();
                PointPairList ATList = new PointPairList();
                PointPairList PWList = new PointPairList();
                PointPairList PBList = new PointPairList();
                PointPairList APList = new PointPairList();

                foreach (var line in file.HRData)
                {
                    double? x = line.ParsedTime;
                    double? y = line.HeartRate;
                    double? y2 = line.PBPIndex;
                    double? y3 = line.Speed;
                    double? y4 = line.Cadence;
                    double? y5 = line.Altitude;
                    double? y6 = line.Power;

                    HRList.Add((double)x, (double)y);
                    SDList.Add((double)x, (double)y3);
                    CDList.Add((double)x, (double)y4);
                    ATList.Add((double)x, (double)y5);
                    PWList.Add((double)x, (double)y6);
                    PBList.Add((double)x, (double)y2);
                }

                if (checkBox1.Checked)
                {
                    myPane.YAxisList.Add(YAxisHR);
                    // Generate a curve
                    LineItem hr = new LineItem("Heart Rate", HRList, Color.Red, SymbolType.None)
                    {
                        YAxisIndex = myPane.YAxisList.IndexOf(YAxisHR),
                        IsY2Axis = false
                    };

                    myPane.CurveList.Add(hr);
                    YAxisHR.IsVisible = true;
                }

                if (checkBox2.Checked)
                {
                    myPane.YAxisList.Add(YAxisSpeed);
                    // Generate a curve
                    LineItem speed = new LineItem("Speed", SDList, Color.Green, SymbolType.None)

                    {
                        YAxisIndex = myPane.YAxisList.IndexOf(YAxisSpeed),
                        IsY2Axis = false
                    };

                    myPane.CurveList.Add(speed);
                    YAxisSpeed.IsVisible = true;
                }

                if (checkBox8.Checked)
                {
                    myPane.Y2AxisList.Add(Y2AxisCadence);
                    // Generate a curve
                    LineItem cadence = new LineItem("Cadence", CDList, Color.Blue, SymbolType.None)
                    {
                        YAxisIndex = myPane.Y2AxisList.IndexOf(Y2AxisCadence),
                        IsY2Axis = true
                    };

                    myPane.CurveList.Add(cadence);
                    Y2AxisCadence.IsVisible = true; ;
                }

                if (checkBox7.Checked)
                {
                    myPane.Y2AxisList.Add(Y2AxisAltitude);
                    // Generate a curve
                    LineItem altitude = new LineItem("Altitude", ATList, Color.Orange, SymbolType.None)
                    {
                        YAxisIndex = myPane.Y2AxisList.IndexOf(Y2AxisAltitude),
                        IsY2Axis = true
                    };

                    myPane.CurveList.Add(altitude);
                    Y2AxisAltitude.IsVisible = true;
                }

                if (checkBox12.Checked)
                {
                    myPane.Y2AxisList.Add(Y2AxisPower);
                    // Generate a curve
                    LineItem power = new LineItem("Power", PWList, Color.Indigo, SymbolType.None)
                    {
                        YAxisIndex = myPane.Y2AxisList.IndexOf(Y2AxisPower),
                        IsY2Axis = true
                    };

                    myPane.CurveList.Add(power);
                    Y2AxisPower.IsVisible = true;
                }

                if (checkBox11.Checked)
                {
                    myPane.YAxisList.Add(YAxisPowerBalance);
                    // Generate a curve
                    LineItem pb = new LineItem("Power Balance", PBList, Color.LightPink, SymbolType.None)
                    {
                        YAxisIndex = myPane.YAxisList.IndexOf(YAxisPowerBalance),
                        IsY2Axis = false
                    };

                    myPane.CurveList.Add(pb);
                    YAxisPowerBalance.IsVisible = true;
                }

                // Calculate Normalised Power
                /*int rolling = 30;
                int total = 0;
                int avg = 0;
                int interval = file.Params.Interval;
                int sum = 0;
                int raise = 4;

                List<int> RollingAverages = new List<int>();
                foreach (LineItem li in myPane.CurveList)
                {
                    for (int i = 0; i < rolling; i++)
                    {
                        if (li.Label.Text.Equals("Power"))
                        {
                            total += (int)li[i].Y;

                            RollingAverages.Add((int)li[i].Y * interval);

                        }
                    }
                }

                int mode = RollingAverages.GroupBy(x => x).OrderByDescending(x => x.Count()).First().Key;

                Console.WriteLine();

                avg = sum / rolling;
                sum = (int)Math.Pow(avg, raise);*/

            }

            if (comparisonFile != null)
            {
                // Creating data points based on the file parser
                PointPairList HRListComparison = new PointPairList();
                PointPairList SDListComparison = new PointPairList();
                PointPairList CDListComparison = new PointPairList();
                PointPairList ATListComparison = new PointPairList();
                PointPairList PWListComparison = new PointPairList();
                PointPairList PBListComparison = new PointPairList();
                PointPairList APListComparison = new PointPairList();

                foreach (var line in comparisonFile.HRData)
                {
                    double? x = line.ParsedTime;
                    double? y = line.HeartRate;
                    double? y2 = line.PBPIndex;
                    double? y3 = line.Speed;
                    double? y4 = line.Cadence;
                    double? y5 = line.Altitude;
                    double? y6 = line.Power;

                    HRListComparison.Add((double)x, (double)y);
                    SDListComparison.Add((double)x, (double)y3);
                    CDListComparison.Add((double)x, (double)y4);
                    ATListComparison.Add((double)x, (double)y5);
                    PWListComparison.Add((double)x, (double)y6);
                    PBListComparison.Add((double)x, (double)y2);
                }

                int alpha = 50;
                var hrcol = Color.Red;
                var spdcol = Color.Green;
                var cadcol = Color.Blue;
                var altcol = Color.Orange;
                var pwrcol = Color.Indigo;
                var pbcol = Color.LightPink;

                if (checkBox1.Checked)
                {
                    // Generate a curve
                    LineItem hr = new LineItem("Heart Rate (Contrast)", HRListComparison, Color.FromArgb(alpha, hrcol), SymbolType.None)
                    {
                        YAxisIndex = myPane.YAxisList.IndexOf(YAxisHR),
                        IsY2Axis = false
                    };

                    myPane.CurveList.Add(hr);
                    YAxisHR.IsVisible = true;
                }

                if (checkBox2.Checked)
                {
                    // Generate a curve
                    LineItem speed = new LineItem("Speed (Contrast)", SDListComparison, Color.FromArgb(alpha, spdcol), SymbolType.None)
                    {
                        YAxisIndex = myPane.YAxisList.IndexOf(YAxisSpeed),
                        IsY2Axis = false
                    };

                    myPane.CurveList.Add(speed);
                    YAxisSpeed.IsVisible = true;
                }

                if (checkBox8.Checked)
                {
                    // Generate a curve
                    LineItem cadence = new LineItem("Cadence (Contrast)", CDListComparison, Color.FromArgb(alpha, cadcol), SymbolType.None)
                    {
                        YAxisIndex = myPane.Y2AxisList.IndexOf(Y2AxisCadence),
                        IsY2Axis = true
                    };

                    myPane.CurveList.Add(cadence);
                    Y2AxisCadence.IsVisible = true; ;
                }

                if (checkBox7.Checked)
                {
                    // Generate a curve
                    LineItem altitude = new LineItem("Altitude (Contrast)", ATListComparison, Color.FromArgb(alpha, altcol), SymbolType.None)
                    {
                        YAxisIndex = myPane.Y2AxisList.IndexOf(Y2AxisAltitude),
                        IsY2Axis = true
                    };

                    myPane.CurveList.Add(altitude);
                    Y2AxisAltitude.IsVisible = true;
                }

                if (checkBox12.Checked)
                {
                    // Generate a curve
                    LineItem power = new LineItem("Power (Contrast)", PWListComparison, Color.FromArgb(alpha, pwrcol), SymbolType.None)
                    {
                        YAxisIndex = myPane.Y2AxisList.IndexOf(Y2AxisPower),
                        IsY2Axis = true
                    };

                    myPane.CurveList.Add(power);
                    Y2AxisPower.IsVisible = true;
                }

                if (checkBox11.Checked)
                {
                    // Generate a curve
                    LineItem pb = new LineItem("Power Balance (Contrast)", PBListComparison, Color.FromArgb(alpha, pbcol), SymbolType.None)
                    {
                        YAxisIndex = myPane.YAxisList.IndexOf(YAxisPowerBalance),
                        IsY2Axis = false
                    };

                    myPane.CurveList.Add(pb);
                    YAxisPowerBalance.IsVisible = true;
                }
            }

            myPane.XAxis.MajorGrid.IsVisible = true;
            myPane.XAxis.MinorGrid.IsVisible = false;

            myPane.YAxis.MajorGrid.IsVisible = true;
            myPane.YAxis.MinorGrid.IsVisible = false;

            myPane.XAxis.Scale.Min = 0;
            myPane.XAxis.Scale.Max = param.Length.TotalSeconds;

            // OPTIONAL: Show tooltips when the mouse hovers over a point
            zg1.IsShowPointValues = true;
            zg1.PointValueEvent += new ZedGraphControl.PointValueHandler(MyPointValueHandler);

            myPane.XAxis.ScaleFormatEvent += new Axis.ScaleFormatHandler(Axis_ScaleFormatEvent);

            // OPTIONAL: Add a custom context menu item
            zg1.ContextMenuBuilder += new ZedGraphControl.ContextMenuBuilderEventHandler(MyContextMenuBuilder);

            // OPTIONAL: Handle the Zoom Event
            zg1.ZoomEvent += new ZedGraphControl.ZoomEventHandler(MyZoomEvent);

            if (myPane.IsZoomed)
            {
            }

            // Tell ZedGraph to calculate the axis ranges
            // Note that you MUST call this after enabling IsAutoScrollRange, since AxisChange() sets
            // up the proper scrolling parameters
            zg1.AxisChange();

            // Make sure the Graph gets redrawn
            zg1.Invalidate();
        }

        /// <summary>
        /// Contains the reference to remove default item from contextual menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="menuStrip"></param>
        /// <param name="mousePt"></param>
        /// <param name="objState"></param>
        private void MyContextMenuBuilder(ZedGraphControl sender, ContextMenuStrip menuStrip, Point mousePt, ZedGraphControl.ContextMenuObjectState objState)
        {
            foreach (ToolStripMenuItem item in menuStrip.Items)
            {
                if ((string)item.Tag == "set_default")
                {
                    menuStrip.Items.Remove(item);
                    break;
                }
            }
        }

        /// <summary>
        /// Checks to the altitude data to see whether user is on static or road bike
        /// </summary>
        /// <param name="param"></param>
        /// <param name="data"></param>
        public void TrainerType(Params param, List<HRData> data)
        {
        }

        /// <summary>
        /// Depending on what unit the user has selected the file will then convert the data based on the metric required
        /// </summary>
        /// <param name="param"></param>
        /// <param name="data"></param>
        /// <param name="trip"></param>
        public void UnitConversion(Params param, List<HRData> data, Trip trip)
        {
            double ConvertedUnit = 0;

            foreach (HRData item in data)
            {
                int i = 0;
                while (i < 1)
                {
                    // Convert the data to metric units (MPH)
                    if (param.sMode.MetricOrImperial == true)
                    {
                        ConvertedUnit = (double)item.Speed * 0.60934;
                        Math.Round(ConvertedUnit);
                        item.Speed = Convert.ToDouble(Math.Round(ConvertedUnit, 2));
                    }
                    else // Changes it to imperial units (KPH)
                    {
                        ConvertedUnit = (double)item.Speed / 0.60934;
                        item.Speed = Convert.ToDouble(Math.Round(ConvertedUnit));
                    }
                    i++;
                }

                zg1.Invalidate();
            }

            int j = 0;
            foreach (HRData item in data)
            {
                ConvertedUnit = 0;

                if (j < 1)
                {
                    if (param.sMode.MetricOrImperial == true)
                    {
                        ConvertedUnit = (double)file.Trip.AvgSpd * 0.60934;
                        file.Trip.AvgSpd = Convert.ToDouble(Math.Round(ConvertedUnit, 2));
                        ConvertedUnit = 0;

                        ConvertedUnit = (double)file.Trip.MaxSpd * 0.60934;
                        file.Trip.MaxSpd = Convert.ToDouble(Math.Round(ConvertedUnit, 2));
                        ConvertedUnit = 0;

                        ConvertedUnit = (double)file.Trip.Distance * 0.60934;
                        file.Trip.Distance = Convert.ToDouble(Math.Round(ConvertedUnit, 2));
                    }
                    else
                    {
                        ConvertedUnit = (double)file.Trip.AvgSpd / 0.60934;
                        file.Trip.AvgSpd = Convert.ToDouble(ConvertedUnit);
                        ConvertedUnit = 0;

                        ConvertedUnit = (double)file.Trip.MaxSpd / 0.60934;
                        file.Trip.MaxSpd = Convert.ToDouble(Math.Round(ConvertedUnit, 2));
                        ConvertedUnit = 0;

                        ConvertedUnit = (double)file.Trip.Distance / 0.60934;
                        file.Trip.Distance = Convert.ToDouble(Math.Round(ConvertedUnit));
                    }
                    j++;
                }
            }

            DataTables(file.HRData, file.Params);
            SummaryData(file.Trip);
        }

        /// <summary>
        /// this methods tells ZedGraph to use the XAxis as a the TimeSpan parssed from data
        /// </summary>
        /// <param name="pane"></param>
        /// <param name="axis"></param>
        /// <param name="val"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        private string Axis_ScaleFormatEvent(GraphPane pane, Axis axis, double val, int index)
        {
            TimeSpan timeVal = TimeSpan.FromSeconds(val); return timeVal.ToString();
        }

        /// <summary>
        /// The region is where the program toggles the visibility of the axis lines
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        #region start of graph attributes allowing user to toggle data

        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            zg1.GraphPane.CurveList.Clear();
            zg1.GraphPane.YAxisList.Clear();
            zg1.GraphPane.Y2AxisList.Clear();
            PlotGraph(file.Params, file.HRData);
        }

        private void CheckBox11_CheckedChanged(object sender, EventArgs e)
        {
            zg1.GraphPane.CurveList.Clear();
            zg1.GraphPane.YAxisList.Clear();
            zg1.GraphPane.Y2AxisList.Clear();
            PlotGraph(file.Params, file.HRData);
        }

        private void CheckBox12_CheckedChanged(object sender, EventArgs e)
        {
            zg1.GraphPane.CurveList.Clear();
            zg1.GraphPane.YAxisList.Clear();
            zg1.GraphPane.Y2AxisList.Clear();
            PlotGraph(file.Params, file.HRData);
        }

        private void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            zg1.GraphPane.CurveList.Clear();
            zg1.GraphPane.YAxisList.Clear();
            zg1.GraphPane.Y2AxisList.Clear();
            PlotGraph(file.Params, file.HRData);
        }

        private void CheckBox7_CheckedChanged(object sender, EventArgs e)
        {
            zg1.GraphPane.CurveList.Clear();
            zg1.GraphPane.YAxisList.Clear();
            zg1.GraphPane.Y2AxisList.Clear();
            PlotGraph(file.Params, file.HRData);
        }

        private void CheckBox8_CheckedChanged(object sender, EventArgs e)
        {
            zg1.GraphPane.CurveList.Clear();
            zg1.GraphPane.YAxisList.Clear();
            zg1.GraphPane.Y2AxisList.Clear();
            PlotGraph(file.Params, file.HRData);
        }

        #endregion start of graph attributes allowing user to toggle data

        /// <summary>
        /// Checks to see whether the select unit is in EU or US when dropdown is closed then passes info to methods
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox1_DropDownClosed(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex != Convert.ToInt32(file.Params.sMode.MetricOrImperial))
            {
            }
            else
            {
                if (comboBox1.Text.Equals("US"))
                {
                    file.Params.sMode.MetricOrImperial = true;
                    UnitConversion(file.Params, file.HRData, file.Trip);
                    LabelUpdater(file.Params);
                }

                if (comboBox1.Text.Equals("EU"))
                {
                    file.Params.sMode.MetricOrImperial = false;
                    UnitConversion(file.Params, file.HRData, file.Trip);
                    LabelUpdater(file.Params);
                }
            }
        }

        /// <summary>
        /// Populates a table and the source within the program where user can see all raw data
        /// </summary>
        /// <param name="data"></param>
        /// <param name="param"></param>
        private void DataTables(List<HRData> data, Params param)
        {
            var list = new BindingList<HRData>(data);

            dataGridView1.DataSource = list;
            dataGridView1.Columns[1].Visible = false;
            dataGridView1.Columns[8].Visible = false;
        }

        /// <summary>
        /// Closes application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// When mouse enters area the file name will be expanded should the name be longer than 17 characters
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void file_title_MouseEnter(object sender, EventArgs e)
        {
            file_title.Text = file.FileName;
        }

        /// <summary>
        /// Checks to see whether the mouse is within a specific area to hide the title of the file for cleaner UI
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void file_title_MouseLeave(object sender, EventArgs e)
        {
            string shortTitle = file.FileName.Substring(0, 17);
            file_title.Text = shortTitle + "...";
        }

        /// <summary>
        /// Added small section for improvement of UI
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            GraphPane PreLoad = zg1.GraphPane;
            PreLoad.Title.Text = "Awaiting Data...";
            PreLoad.XAxis.Title.Text = "";
            PreLoad.YAxis.Title.Text = "";
            openComparisonToolStripMenuItem.Enabled = false;
        }

        /// <summary>
        /// Accepts the file selected by the user, parses it and sends it to multiple methods validating and parsing
        /// </summary>
        /// <param name="settingFile"></param>
        /// <param name="settingComparisonFile"></param>
        /// <param name="pathToFile"></param>
        private void HandleFile(bool settingFile, bool settingComparisonFile, string pathToFile)
        {
            string FileData = string.Empty;
            string HRData = string.Empty;
            string HRZones = string.Empty;
            string IntNotes = string.Empty;
            string IntTimes = string.Empty;
            string LapNames = string.Empty;
            int lineCount = 1;
            string Note = string.Empty;
            string Params = string.Empty;
            IFParser parser;

            string Summary123 = string.Empty;
            string SummaryTH = string.Empty;
            string SwapTimes = string.Empty;
            string Trip = string.Empty;
            int chunk = Convert.ToInt32(numericUpDown1.Value);

            if (File.Exists(pathToFile))// only executes if the file at pathtofile exists
            {
                ImportedFile newFile;

                using (StreamReader sr = new StreamReader(pathToFile))
                {
                    newFile = new ImportedFile();

                    GC.Collect();

                    // Get only the name of the file from the full path.
                    pathToFile = Path.GetFileName(pathToFile);

                    newFile.FileName = pathToFile;

                    parser = new IFParser(newFile);

                    FileData = sr.ReadLine();

                    if (FileData == "[Params]")
                    {
                        // Collection of all data that follows [Params]
                        while (FileData != (""))
                        {
                            FileData = sr.ReadLine();
                            Params += FileData + "\n";
                            lineCount++;
                        }
                        FileData = sr.ReadLine();
                        while (FileData != (""))
                        {
                            FileData = sr.ReadLine();
                            Note += FileData + "\n";
                            lineCount++;
                        }
                        FileData = sr.ReadLine();
                        while (FileData != (""))
                        {
                            FileData = sr.ReadLine();
                            IntTimes += FileData + "\n";
                            lineCount++;
                        }
                        FileData = sr.ReadLine();
                        while (FileData != (""))
                        {
                            FileData = sr.ReadLine();
                            IntNotes += FileData + "\n";
                            lineCount++;
                        }
                        FileData = sr.ReadLine();
                        while (FileData != (""))
                        {
                            FileData = sr.ReadLine();
                            ExtraData += FileData + "\n";
                            lineCount++;
                        }
                        FileData = sr.ReadLine();
                        while (FileData != (""))
                        {
                            FileData = sr.ReadLine();
                            LapNames += FileData + "\n";
                            lineCount++;
                        }
                        FileData = sr.ReadLine();
                        while (FileData != (""))
                        {
                            FileData = sr.ReadLine();
                            Summary123 += FileData + "\n";
                            lineCount++;
                        }
                        FileData = sr.ReadLine();
                        while (FileData != (""))
                        {
                            FileData = sr.ReadLine();
                            SummaryTH += FileData + "\n";
                            lineCount++;
                        }
                        FileData = sr.ReadLine();
                        while (FileData != (""))
                        {
                            FileData = sr.ReadLine();
                            HRZones += FileData + "\n";
                            lineCount++;
                        }
                        FileData = sr.ReadLine();
                        while (FileData != (""))
                        {
                            FileData = sr.ReadLine();
                            SwapTimes += FileData + "\n";
                            lineCount++;
                        }
                        FileData = sr.ReadLine();
                        while (FileData != ("[HRData]") && !string.IsNullOrEmpty(FileData) && !string.IsNullOrWhiteSpace(FileData))
                        {
                            FileData = sr.ReadLine();
                            Trip += FileData + "\n";
                            lineCount++;
                        }
                        FileData = sr.ReadLine();
                        while (FileData != null)
                        {
                            FileData = sr.ReadLine();
                            HRData += FileData + "\n";
                            lineCount++;
                        }

                        newFile.Params = parser.ParseParams(Params);
                        newFile.HRData = parser.ParseHRData(HRData);
                        newFile.IntNotes = parser.ParseIntNotes(IntNotes);
                        newFile.Trip = parser.ParseTrip(Trip);

                        if (settingFile)
                        {
                            file = newFile;
                        }
                        else if (settingComparisonFile)
                        {
                            comparisonFile = newFile;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Sorry but the file you have selected from: \n" + pathToFile + "\n is not a valid HRM file", "Invalid Filetype", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    button1.Text = "Import Comparison";
                }
            }

            UpdateControls();
        }

        /// <summary>
        /// Updates all of the labels in the top info bar in the application giving user quick info about session
        /// </summary>
        /// <param name="param"></param>
        private void LabelUpdater(Params param)
        {
            if (file.FileName.Length > 17)
            {
                string shortTitle = file.FileName.Substring(0, 17);
                file_title.Text = shortTitle + "...";
                file_title.Visible = true;
            }
            label12.Text = param.StartTime.ToString();
            label12.Visible = true;
            date_label.Text = param.Date.ToString();
            date_label.Visible = true;
            version_label.Text = param.Version.ToString();
            version_label.Visible = true;
            date_label.Text = param.Date.ToString();
            date_label.Visible = true;
            v02_label.Text = param.V02max.ToString();
            v02_label.Visible = true;
            hr_label.Text = param.MaxHR.ToString();
            hr_label.Visible = true;
            rest_label.Text = param.RestHR.ToString();
            rest_label.Visible = true;
            label13.Text = param.Length.ToString("hh':'mm':'ss");
            label13.Visible = true;
            label14.Text = param.StartTime.Add(param.Length).ToString("hh':'mm':'ss");
            label14.Visible = true;
            if (file.Params.sMode.MetricOrImperial == false)
            {
                label15.Text = file.Trip.Distance.ToString() + " Km";
                comboBox1.SelectedIndex = 1;
            }
            else
            {
                label15.Text = file.Trip.Distance.ToString() + " Miles";
                comboBox1.SelectedIndex = 0;
            }
            label15.Visible = true;
        }

        /// <summary>
        /// Deals with modifying the hover over action where specific information is given on that line with regards to information hovered over
        /// </summary>
        /// <param name="control"></param>
        /// <param name="pane"></param>
        /// <param name="curve"></param>
        /// <param name="iPt"></param>
        /// <returns></returns>
        private string MyPointValueHandler(ZedGraphControl control, GraphPane pane, CurveItem curve, int iPt)
        {
            PointPair pt = curve[iPt];
            double xAxisMin = pane.XAxis.Scale.Min;
            double xAxisMax = pane.XAxis.Scale.Max;

            string ContextLabel = null;

            #region area for the inital files prior to copmarison

            // Get the PointPair that is under the mouse
            if (curve.Label.Text.Equals("Power"))
            {
                ContextLabel = curve.Label.Text + " is " + pt.Y.ToString() + " Watts at " + pt.X.ToString() + " seconds";
            }
            else if (curve.Label.Text.Equals("Heart Rate"))
            {
                ContextLabel = curve.Label.Text + " is " + pt.Y.ToString() + " BPM at " + pt.X.ToString() + " seconds";
            }
            else if (curve.Label.Text.Equals("Speed"))
            {
                string unit = null;

                if (file.Params.sMode.MetricOrImperial == false)
                {
                    unit = " KPH";
                }
                else
                {
                    unit = " MPH";
                }

                ContextLabel = curve.Label.Text + " is " + pt.Y.ToString() + unit + " at " + pt.X.ToString() + " seconds";
            }
            else if (curve.Label.Text.Equals("Cadence"))
            {
                ContextLabel = curve.Label.Text + " is " + pt.Y.ToString() + " RPM at " + pt.X.ToString() + " seconds";
            }
            else if (curve.Label.Text.Equals("Altitude"))
            {
                string unit = null;

                if (file.Params.sMode.MetricOrImperial == false)
                {
                    unit = " ft";
                }
                else
                {
                    unit = " m";
                }

                ContextLabel = curve.Label.Text + " is " + pt.Y.ToString() + unit + " at " + pt.X.ToString() + " seconds";
            }
            else if (curve.Label.Text.Equals("Power Balance"))
            {
                ContextLabel = curve.Label.Text + " is " + pt.Y.ToString() + " at " + pt.X.ToString() + " seconds";
            }

            #endregion area for the inital files prior to copmarison

            #region area for the comparison axis

            // Get the PointPair that is under the mouse
            if (curve.Label.Text.Equals("Power (Contrast)"))
            {
                ContextLabel = curve.Label.Text + " is " + pt.Y.ToString() + " Watts at " + pt.X.ToString() + " seconds";
            }
            else if (curve.Label.Text.Equals("Heart Rate (Contrast)"))
            {
                ContextLabel = curve.Label.Text + " is " + pt.Y.ToString() + " BPM at " + pt.X.ToString() + " seconds";
            }
            else if (curve.Label.Text.Equals("Speed (Contrast)"))
            {
                string unit = null;

                if (file.Params.sMode.MetricOrImperial == false)
                {
                    unit = " KPH";
                }
                else
                {
                    unit = " MPH";
                }

                ContextLabel = curve.Label.Text + " is " + pt.Y.ToString() + unit + " at " + pt.X.ToString() + " seconds";
            }
            else if (curve.Label.Text.Equals("Cadence (Contrast)"))
            {
                ContextLabel = curve.Label.Text + " is " + pt.Y.ToString() + " RPM at " + pt.X.ToString() + " seconds";
            }
            else if (curve.Label.Text.Equals("Altitude (Contrast)"))
            {
                string unit = null;

                if (file.Params.sMode.MetricOrImperial == false)
                {
                    unit = " ft";
                }
                else
                {
                    unit = " m";
                }

                ContextLabel = curve.Label.Text + " is " + pt.Y.ToString() + unit + " at " + pt.X.ToString() + " seconds";
            }
            else if (curve.Label.Text.Equals("Power Balance (Contrast)"))
            {
                ContextLabel = curve.Label.Text + " is " + pt.Y.ToString() + " at " + pt.X.ToString() + " seconds";
            }

            #endregion area for the comparison axis

            return ContextLabel;
        }

        /// <summary>
        /// Zoom event that allows the user to zoom the graph
        /// </summary>
        /// <param name="control"></param>
        /// <param name="oldState"></param>
        /// <param name="newState"></param>
        private void MyZoomEvent(ZedGraphControl control, ZoomState oldState, ZoomState newState)
        {

        }

        /// <summary>
        /// Opens toolstrip in top of program
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Button1_Click(null, null);
        }

        /// <summary>
        /// scans the valid points and checks to see what intervals are within the graph
        /// </summary>
        /// <param name="Trip"></param>
        private void IntervalData(Trip Trip)
        {
            ImportedFile imported = null;
            DataTable IntSrc = new DataTable();
            string int_col = "Interval" ; string start_col = "Start"; string end_col = "End";
            IntSrc.Columns.Add().ColumnName = int_col;
            IntSrc.Columns.Add().ColumnName = start_col;
            IntSrc.Columns.Add().ColumnName= end_col;
            IntSrc.Rows.Clear();
            string start = string.Empty;
            string end = string.Empty;
            int parsedData = 0;
            double RAvg = 0;

            if (file != null) parsedData++;
            if (comparisonFile != null) parsedData++;

            for (int i = 0; i < parsedData; i++)
            {
                if (i == 0) imported = file;
                if (i == 1) imported = comparisonFile;

                if (imported != null)
                {
                    int intervalCount = 0;

                    double flatten = 6; double boundary = trackBar1.Value; double PAvg = 0; double thisPoint = 0; bool StartPoint = true;
                    double endPoint = imported.HRData.Count;
                    string interval = string.Empty;

                    for (int j = 0; j < file.HRData.Count; j++)
                    {
                        thisPoint = j;
                        PAvg = RAvg;

                        if ((thisPoint + flatten) > endPoint)
                        {
                            break;
                        }
                        else
                        {
                            for (int k = 0; k < flatten; k++)
                            {
                                RAvg += (double)imported.HRData[(int)(thisPoint + k)].Power;
                            }
                            RAvg = RAvg / flatten;

                            if (StartPoint == true)
                            {
                                if (RAvg > imported.HRData[(int)thisPoint].Power + ((imported.HRData[(int)thisPoint].Power / 100) * boundary))
                                {
                                    intervalCount++;
                                    interval = intervalCount.ToString();

                                    start = TimeSpan.FromSeconds(imported.HRData[(int)thisPoint].ParsedTimeFormatted.Seconds).ToString();

                                    StartPoint = false;
                                }
                            }

                            if (StartPoint == false)
                            {
                                if (RAvg < imported.HRData[(int)thisPoint].Power - ((imported.HRData[(int)thisPoint].Power / 100) * 15))
                                {
                                    end = TimeSpan.FromSeconds(imported.HRData[(int)thisPoint].ParsedTimeFormatted.Seconds).ToString();

                                    //Console.WriteLine(TimeSpan.FromSeconds(hrm.HRDRowCollector[(int)thisPoint].Seconds));
                                    StartPoint = true;
                                }
                            }
                            if (interval != null && start != null && end != null)
                            {
                                int time = start.CompareTo(end);
                                if (time > 0)
                                {
                                    IntSrc.Rows.Add(interval, end, start);
                                    dataGridView3.DataSource = IntSrc;
                                    RemoveDuplicateRows(IntSrc, int_col);
                                }

                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Checks thew data thats passed into any colums and strips the duplicates for better ui
        /// </summary>
        /// <param name="IntSrc"></param>
        /// <param name="colName"></param>
        /// <returns></returns>
        public DataTable RemoveDuplicateRows(DataTable IntSrc, string colName)
        {
            Hashtable hTable = new Hashtable();
            ArrayList duplicateList = new ArrayList();

            //Add list of all the unique item value to hashtable, which stores combination of key, value pair.
            //And add duplicate item value in arraylist.
            foreach (DataRow drow in IntSrc.Rows)
            {
                if (hTable.Contains(drow[colName]))
                    duplicateList.Add(drow);
                else
                    hTable.Add(drow[colName], string.Empty);
            }

            //Removing a list of duplicate items from datatable.
            foreach (DataRow dRow in duplicateList)
                IntSrc.Rows.Remove(dRow);

            //Datatable which contains unique records will be return as output.
            return IntSrc;
        }

        /// <summary>
        /// Takes teh summary data from the HR Monitor, also chunk that average data as a whole (not yet as individual sections)
        /// </summary>
        /// <param name="Trip"></param>
        private void SummaryData(Trip Trip)
        {
            int chunk = Convert.ToInt32(numericUpDown1.Value);

            int AvgHR = (file.Params.MaxHR + file.Params.RestHR) / 2;
            var SummarySrc = new DataTable();

            SummarySrc.Columns.Add("Distance");
            SummarySrc.Columns.Add("Ascent");
            SummarySrc.Columns.Add("Total Time (s)");
            SummarySrc.Columns.Add("Avg Altitude");
            SummarySrc.Columns.Add("Max Altitude");
            SummarySrc.Columns.Add("Avg Speed");
            SummarySrc.Columns.Add("Max Speed");
            SummarySrc.Columns.Add("Avg Heart Rate");
            SummarySrc.Columns.Add("Max Heart rate");
            SummarySrc.Columns.Add("Min Heart Rate");
            SummarySrc.Columns.Add("Odometer");


            for (int i = 0; i < chunk; i++)
            {
                SummarySrc.Rows.Add(Trip.Distance / chunk, Trip.Ascent / chunk, Trip.TTIS / chunk, Trip.AvgAlt / chunk, Trip.MaxAlt / chunk, Trip.AvgSpd / chunk, Trip.MaxSpd / chunk, AvgHR / chunk, file.Params.MaxHR / chunk, file.Params.RestHR / chunk, Trip.Odo / chunk);
                dataGridView2.DataSource = SummarySrc;
            }
        }

        /// <summary>
        /// Check to see whether user has given a maximum heart rate and also validates input
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBox1_Leave(object sender, EventArgs e)
        {
            string max_given_hr = textBox1.Text;

            if (max_given_hr != "")
            {
                if (comboBox2.SelectedIndex == 0)
                {
                    // Graph Mods
                    /*zg1.GraphPane.YAxis.Scale.Max = Convert.ToDouble(max_given_hr);
                    zg1.Refresh(); */

                    file.Params.MaxHR = Convert.ToInt32(max_given_hr);
                    hr_label.Text = max_given_hr;
                    SummaryData(file.Trip);
                }
                else
                {
                    if (comboBox2.SelectedIndex == 1 && Convert.ToInt32(max_given_hr) > 100)
                    {
                        MessageBox.Show("Sorry, you can't enter a value greater than 100 when the percentile option is selected", "Value Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        textBox1.Text = 100.ToString();
                    }

                    int CustomHR;
                    CustomHR = Convert.ToInt32(hr_label.Text) / 100 * Convert.ToInt32(max_given_hr);
                    file.Params.MaxHR = CustomHR;
                    SummaryData(file.Trip);
                }
            }
            else
            {
                zg1.GraphPane.YAxis.Scale.Max = 700;
                zg1.Refresh();
            }
        }

        /// <summary>
        /// Check to see whether the user has given a max ftp
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBox2_TextChanged(object sender, EventArgs e)
        {
            double? max_ftp_given = Convert.ToDouble(numericUpDown1.Text);
        }

        /// <summary>
        /// Used to reload the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        /// <summary>
        /// Checks to see whether multiple files have been imported
        /// </summary>
        private void UpdateControls()
        {
            if (file != null)
            {
                TrainerType(file.Params, file.HRData);
                LabelUpdater(file.Params);
                PlotGraph(file.Params, file.HRData);
                DataTables(file.HRData, file.Params);
                SummaryData(file.Trip);
                IntervalData(file.Trip);
            }
        }

        private void Zg1_Load(object sender, EventArgs e)
        {
            GraphPane myPane = zg1.GraphPane;
        }

        /// <summary>
        /// Awaits value change to send instruction to another method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            SummaryData(file.Trip);
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            IntervalData(file.Trip);
        }
    }
}